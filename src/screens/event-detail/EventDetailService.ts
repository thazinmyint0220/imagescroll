import { executeQuery } from "../../aws/db/dbOperation";

// GET VenueName
export const getVenueNameAndId = async (eventDetailId: number) => {
  const method = "POST";
  const queryString =
    "SELECT name, venue_id FROM venue where event_id = " + eventDetailId + ";";
  return executeQuery(method, queryString);
};
