import { format } from "date-fns";
import { executeQuery } from "../../aws/db/dbOperation";
// get Data from gender name
export const fetchGenderName = async (genderCode: string) => {
  const method = "POST";
  const queryString =
    "SELECT name FROM gender WHERE gender_code ='" + genderCode + "';";
  return executeQuery(method, queryString);
};

//insert reception
export const insertReceptionData = async (
  cityCode: string,
  eventId: number,
  venueId: number,
  memberId: number,
  modifierId: string,
  lgapId: string,
  lastName: string,
  firstName: string,
  lastNameKana: string,
  firstNameKana: string,
  dateOfBirth: Date | null,
  genderCode: string,
  postalCode: string,
  address: string,
  receptionTypeCode: string,
  familyOrderNumber: number
) => {
  let formattedDateOfBirth: string;
  if (dateOfBirth) {
    formattedDateOfBirth = format(dateOfBirth, "yyyy-MM-dd");
  } else {
    formattedDateOfBirth = "";
  }

  const method = "POST";
  const queryString = `INSERT INTO reception (
    city_code, event_id, venue_id, member_id, history_number, accepted_terminal_id, accepted_timestamp, modifier_id, modification_timestamp, is_deleted, lgap_id, user_rank, lastname, firstname, lastname_kana, firstname_kana, date_of_birth, gender_code, postal_code, address, relationship, reception_type_code, family_order_number
)
VALUES (
    '242152',
    ${eventId},
    ${venueId}, 
    0, 
    0,
    NULL,
    CURRENT_TIMESTAMP, 
    '${modifierId}', 
    CURRENT_TIMESTAMP, 
    false,
    CASE
      WHEN '${lgapId}' ='' THEN NULL
      ELSE '${lgapId}'
    END,
	'4',
    '${lastName}',
    '${firstName}', 
    '${lastNameKana}',
    '${firstNameKana}',
    '${formattedDateOfBirth}',
    '${genderCode}',
    '${postalCode}', 
    '${address}', 
    '本人', 
    '${receptionTypeCode}', 
    0 );`;
  return executeQuery(method, queryString);
};
